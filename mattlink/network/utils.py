#!/usr/bin/python
# -*- coding: utf-8 -*-
from operator import itemgetter


def intersect(lists):
    """ Effectue l'intersection d'un nombre quelconque de listes """
    lists.append([])
    return list(set.intersection(*map(set, lists)))


def count_occurences(lst):
    """ Compte pour chaque objet de la liste, le nombre de fois qu'il apparait dans la liste. """
    objects_count = {}

    for elem in lst:
        if elem in objects_count:
            objects_count[elem] = objects_count[elem] + 1
        else:
            objects_count[elem] = 1

    return objects_count


def n_keys_with_largest_values(d, n):
    """ Renvoie les n clés du dictionnaire d telles que les valeurs correspondantes soient les plus grandes """
    return [x[0] for x in sorted(d.items(), key=itemgetter(1), reverse=True)][:n]
