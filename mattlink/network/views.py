#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages

from models import User, Post, Tag


def is_user_logged(request):
    return 'username' in request.session


def get_user(request):
    """ Renvoie une instance de l'utilisateur actuellement connecté """
    return User.objects.get(username=request.session['username'])


def get_minimal_context(request):
    """ Factorise les informations minimales pour chaque page lorsque l'utilisateur est connecté """
    context = dict()
    if is_user_logged(request):
        context['username'] = request.session['username']

    return context


def index(request):
    context = get_minimal_context(request)

    if is_user_logged(request):
        u = get_user(request)

        context['posts'] = u.get_recent_posts()
        context['suggested_tags'] = u.suggest_tags()
        context['favorite_tags'] = u.get_favorite_tags()
        context['suggested_users_by_links'] = u.people_suggest_by_links()
        context['suggested_users_by_tags'] = u.people_suggest_by_tags()
        context['suggested_posts'] = u.suggest_posts()
        context['followed_users'] = u.get_followed_users()

    return render(request, 'index.html', context)


def subscribe(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')

        if not (username and email and password):
            messages.error('Il faut remplir tout les champs')
            return render(request, 'subscribe.html')
        else:
            try:
                User.objects.get(username=username)
                messages.error('Nom d\'utilisateur déjà pris')
                return render(request, 'subscribe.html')

            except ObjectDoesNotExist:
                new_user = User(username=username, email=email, password=password)
                new_user.save()

            messages.success(request, 'l\'inscription s\'est bien déroulée, vous pouvez maintenant vous connecter')
            return HttpResponseRedirect('/')
    else:
        return render(request, 'subscribe.html')


def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        try:
            user = User.objects.get(username=username, password=password)
            request.session['username'] = user.username
        except ObjectDoesNotExist:
            messages.error(request, 'Nom ou mot de passe incorrect')
            return render(request, 'login.html')

        messages.success(request, 'Connexion réussie')
        return HttpResponseRedirect('/')

    return render(request, 'login.html')


def logout(request):
    try:
        del request.session['username']
    except KeyError:
        pass

    return HttpResponseRedirect('/')


def tag(request, tag_label):
    context = get_minimal_context(request)

    try:
        tag = Tag.objects.get(label=tag_label)

        context['tag'] = tag
        context['posts'] = tag.posts.all()
        context['users'] = tag.get_users()
        context['tags'] = tag.get_similar_tags()

    except ObjectDoesNotExist:
        return HttpResponseRedirect('/search/%s' % tag_label)

    return render(request, 'tag.html', context)


def search(request):
    context = get_minimal_context(request)
    term = request.POST.get('search_term')

    try:
        Tag.objects.get(label=term)
        return HttpResponseRedirect('/tag/%s' % term)
    except ObjectDoesNotExist:
        try:
            User.objects.get(username=term)
            return HttpResponseRedirect('/tag/%s' % term)
        except ObjectDoesNotExist:
            messages.error(request, u"La recherche n'a donné aucun résultat : %s" % term)
            return render(request, 'search.html', context)


def user(request, username):
    context = get_minimal_context(request)
    try:
        current_u = get_user(request)
        user = User.objects.get(username=username)

        context['user'] = user.username
        context['favorite_tags'] = user.get_favorite_tags()
        context['posts'] = user.get_recent_posts()
        context['is_followed'] = user in current_u.followed.all()
        context['followed_users'] = user.get_followed_users()

    except ObjectDoesNotExist:
        messages.error(request, "L'utilisateur %s n'existe pas" % username)
        return HttpResponseRedirect('/')

    return render(request, 'user.html', context)


def follow(request, username):
    try:
        follower = get_user(request)
        followed = User.objects.get(username=username)

        follower.followed.add(followed)
        follower.save()

        return HttpResponseRedirect('/user/{username}'.format(username=username))
    except ObjectDoesNotExist:
        render(request, 'index.html')


def comment(request, post_id):
    pass


def like(request, post_id):
    try:
        post = Post.objects.get(id=post_id)
        user = get_user(request)

        if user in post.dislikers.all():
            post.dislikers.remove(user)
            post.nb_dislike -= 1

        post.likers.add(user)
        post.nb_like += 1

        user.update_interest(post.get_tags(), like=True)
        post.save()
        user.save()

    except ObjectDoesNotExist:
        messages.error('Une erreur est survenue inopinément')

    return HttpResponseRedirect('/')


def dislike(request, post_id):
    try:
        post = Post.objects.get(id=post_id)
        user = get_user(request)

        if user in post.likers.all():
            post.likers.remove(user)
            post.nb_like -= 1

        post.dislikers.add(user)
        post.nb_dislike += 1

        user.update_interest(post.get_tags(), like=False)

        post.save()
        user.save()

    except ObjectDoesNotExist:
        messages.error('Une erreur est survenue inopinément')

    return HttpResponseRedirect('/')


def newpost(request):
    if request.method == 'POST':
        title = request.POST.get('title')
        content = request.POST.get('content')

        try:
            owner = User.objects.get(username=request.session['username'])
            Post.create(title=title, content=content, owner=owner).save()

        except ObjectDoesNotExist:
            messages.error('Une erreur est survenue inopinément')
            return render(request, 'login.html')

        messages.success(request, 'Partage enregistré')
        return HttpResponseRedirect('/')

    return render(request, 'login.html')
