from django.conf.urls import patterns, url

from network import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^subscribe/$', views.subscribe, name='subscribe'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^tag/(?P<tag_label>\w*)/$', views.tag, name='tag'),
    url(r'^search/$', views.search, name='search'),
    url(r'^user/(?P<username>\w*)/$', views.user, name='user'),
    url(r'^user/(?P<username>\w*)/follow$', views.follow, name='follow'),
    url(r'^newpost/$', views.newpost, name='newpost'),
    url(r'^like/(?P<post_id>\d+)/$', views.like, name='like'),
    url(r'^dislike/(?P<post_id>\d+)/$', views.dislike, name='dislike'),
    url(r'^comment/(?P<post_id>\d+)/$', views.comment, name='comment'),
)
