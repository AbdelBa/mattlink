#!/usr/bin/python
# -*- coding: utf-8 -*-
from datetime import datetime
from random import randint

from django.core.exceptions import ObjectDoesNotExist

from neo4django.db import models

import utils


class User(models.NodeModel):
    username = models.StringProperty()
    email = models.EmailProperty()
    password = models.StringProperty()

    followed = models.Relationship('self', rel_type='follows', related_name='followeds')
    posts = models.Relationship('Post', rel_type='owns', related_name='owner')
    tags = models.Relationship('Tag', rel_type='interested', related_name='users')

    def get_tags(self):
        return set(self.tags.all())

    def get_followed_users(self):
        return set(self.followed.all())

    def get_interest_by_tag(self):
        return utils.count_occurences(self.get_tags())

    def get_recent_posts(self):
        posts = self.posts.all()[:15]

        for post in posts:
            if post in self.liked_posts.all():
                post.liked = True

            if post in self.disliked_posts.all():
                post.disliked = True

        return posts

    def get_favorite_tags(self):
        tag_interest = self.get_interest_by_tag()

        if tag_interest:  # Pour éviter la division par 0
            threshold = sum(tag_interest.values()) / len(tag_interest)

            fav_tags = [tag for tag in self.get_tags() if tag_interest[tag] >= threshold]
            fav_tags.sort(key=lambda x: tag_interest[x], reverse=True)

            return fav_tags[:10]
        else:
            return []

    def people_suggest_by_links(self):
        followed_of_followed = utils.intersect([user.followed.all() for user in self.followed.all()])
        fav_tags = set(self.get_favorite_tags())
        users = []

        for user in followed_of_followed:
            if fav_tags & set(user.get_favorite_tags):  # Intersection non vide, au moins un tag en commun.
                users.append(user)

        return self.filter_suggested_people(users)[:10]

    def people_suggest_by_tags(self):
        users = []

        for tag in self.get_tags():
            users.extend(tag.get_most_interested_users())

        return set(self.filter_suggested_people(users)[:10])

    def filter_suggested_people(self, users):
        return [u for u in users if (not u in self.get_followed_users()) and u != self]

    def suggest_tags(self):
        tags = filter(None, [tag.get_most_similar_tag() for tag in self.get_favorite_tags()])

        return [tag for tag in tags if tag not in self.get_favorite_tags()]

    def suggest_posts(self):
        return self.posts.all()

    def update_interest(self, tags, like):
        if like:
            for tag in tags:
                self.tags.add(tag)
        else:
            for tag in tags:
                if tag in self.get_tags():
                    self.tags.remove(tag)

    def __unicode__(self):
        return self.username


class Tag(models.NodeModel):
    label = models.StringProperty()

    similar = models.Relationship('self', rel_type='similar', related_name='similar')

    def add_similar_tag(self, tag):
        self.node.relationships.create('similar', tag.node, intensity=0)

    def update_similarity(self, tags):
        for index, tag_a in enumerate(tags):
            for tag_b in tags[index + 1:]:
                tag_a.similar.add(tag_b)
            tag_a.save()

    def get_users(self):
        return set(self.users.all())

    def get_interest_by_user(self):
        return utils.count_occurences(self.get_users())

    def get_most_interested_users(self):
        user_interest = self.get_interest_by_user()
        return utils.n_keys_with_largest_values(user_interest, 5)

    def get_similar_tags(self):
        similarity_by_tags = utils.count_occurences(self.similar.all())

        if (similarity_by_tags):
            return utils.n_keys_with_largest_values(similarity_by_tags, 10)
        else:
            return None

    def get_most_similar_tag(self):
        similarity_by_tags = utils.count_occurences(self.similar.all())

        if (similarity_by_tags):
            return utils.n_keys_with_largest_values(similarity_by_tags, 1)[0]
        else:
            return None

    def __unicode__(self):
        return self.label


class Post(models.NodeModel):
    title = models.StringProperty()
    content = models.StringProperty()
    date = models.DateTimeProperty()
    nb_like = models.IntegerProperty()
    nb_dislike = models.IntegerProperty()

    tags = models.Relationship('Tag', rel_type='occurs', related_name='posts')
    comments = models.Relationship('Comment', rel_type='commented', related_name='post')
    likers = models.Relationship('User', rel_type='liked', related_name='liked_posts')
    dislikers = models.Relationship('User', rel_type='disliked', related_name='disliked_posts')

    @classmethod
    def create(cls, title, content, owner):
        """ La création d'une publication donne lieu à différentes mises à jours au niveau
            des deux autres entités. Il faut donc définir une méthode de création particulière. """
        post = cls(title=title, content=content, date=datetime.now())

        post.extract_tags()
        post.extract_links()
        tags = post.get_tags()

        post.owner.add(owner)
        owner.update_interest(tags, True)

        for i in xrange(len(tags)):
            tags[i].update_similarity(tags[i + 1:])
            owner.tags.add(tags[i])

        owner.save()
        return post

    def get_tags(self):
        return self.tags.all()

    def get_owner(self):
        return self.owner.all()[0]

    def extract_tags(self):
        for word in self.content.split():
            if word.startswith("#"):
                label = word.strip("#").lower()

                # On enlève la ponctuation éventuelle en dernière position.
                if label[-1] in ",!.;?":
                    label = label[:-1]

                try:
                    tag = Tag.objects.get(label=label)
                except ObjectDoesNotExist:
                    tag = Tag.objects.create(label=label)

                self.tags.add(tag)

    def extract_links(self):
        pass

    def __unicode__(self):
        if self.title is None:
            return "No title"
        return self.title


class Comment(models.NodeModel):
    content = models.StringProperty()
    date = models.DateTimeProperty()

    user = models.Relationship('User', rel_type='comment', related_name='comments')

    def __unicode__(self):
        return self.content
